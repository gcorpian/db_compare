#---------- IMPORTS ----------
from subprocess import Popen, PIPE
from collections import OrderedDict
import os
import re
import base64
import pymysql.cursors
import os
import sys
import logging
from logging import FileHandler
from datetime import datetime
import codecs


#---------- VARIABLE DECLARATIONS ----------
indent = "    "    

os.environ['ORACLE_HOME'] = "/opt/oracle/instantclient"
os.environ['DYLD_LIBRARY_PATH'] = "/opt/oracle/instantclient"


#---------- ENVIRONMENT SETUP ----------
#env = "P-0"     #prod
env = "P-1"     #stage
#env = "P-2"     #qa
#env = "P-3"    #dev

#---------- LOG FILE SETUP ----------
resultsfile = "DB_Compare_Log_" + datetime.now().strftime("_%m%d_%H%M")
logfile = resultsfile + "_" + env + '.log'
imagefile = resultsfile + '.png' #move and change to include row number

if sys.platform == "darwin":
    LogFolder = "../Logs/"
    os.system("touch " + LogFolder + logfile)
elif sys.platform == "windows" or sys.platform == "win32":
    LogFolder = "..\\Logs\\"
    # On Windows, must ensure Logs folder exists.
    if not os.path.isdir(LogFolder):
        os.mkdir(LogFolder)

schemaList = ["IDMRW", "IDMRW2","XIDAPPS", "IDDBMONITOR", "CLAIM_ACCOUNT", "ONBOARDAUDIT", "SAILPOINT", "IOMGR", "EMPINST", "IAMUTILITY", "IDMFEED", "PACSDATA", "IDCARDDATA", "IDENTITYAPI"]
#schemaList = ["IDDBMONITOR", "CLAIM_ACCOUNT", "ONBOARDAUDIT", "SAILPOINT", "IOMGR", "EMPINST", "IAMUTILITY", "IDMFEED", "PACSDATA", "IDCARDDATA", "IDENTITYAPI"]
dateValue = {"IDMRW": "UPDATE_DT", "IDMRW2": "UPDATE_DT", "XIDAPPS": "CREATE_DATE", "IDDBMONITOR": "none", "CLAIM_ACCOUNT": "none", "ONBOARDAUDIT": "EVENT_DT", "SAILPOINT": "LAST_UPDATE_DT", "IOMGR": "none", "EMPINST": "none", "IAMUTILITY": "none", "IDMFEED": "none", "PACSDATA": "LAST_UPDATE_TS", "IDCARDDATA": "LAST_UPDATE_TS", "IDENTITYAPI": "none"}

if env == "P-3":
    DBServerOld = 'ii5qn9ltcnf1qj.cmkqwwtrxlfv.us-east-1.rds.amazonaws.com:1521/iamdev'
    #DBServerOld = 'iamdev-rds.dev.iam.harvard.edu:1521/iamdev' ## 11g database
    DBPermissions = {"IAMAUTOQA": "aXRsMXZlaTI=", "CLAIM_ACCOUNT": "aXRsMXZlaTI=", "IDMRW": "aXRsMXZlaTI=", "IDMRW2": "aXRsMXZlaTI=", "SAILPOINT":"dGVhbXcwcms="}
elif env == "P-2":
    DBServerOld = 'iamqa-rds.qa.iam.harvard.edu:1521/iamqa'    
    DBPermissions = {"IAMAUTOQA": "aXRsMXZlaTI=", "IDMRW": "aXRsMXZlaTI=", "IDMRW2": "aXRsMXZlaTI=", "SAILPOINT": "SmF3OW1Ib1Y3OXNGR3FhaWN5TGo1TVhzdTcycDJ3", "GROUPER": "Z3JvdXBlcjEyMzQ1Njc4"}
elif env == "P-1":  #WraketR7
    DBServerOld = '10.32.4.81:8405/iamstg'
    #DBServerNew = 'iicmzcspljwvwf.cnkzi111a9dz.us-east-1.rds.amazonaws.com:1521/iamstg'
    DBServerNew = 'iam-database.stage.iam.harvard.edu:1521/iamstg'
    DBPermissions = {"IAMAUTOQA": "V3Jha2V0Ujc=", "CLAIM_ACCOUNT": "Y2VMTHgyMzc3", "IDMRW": "MjB3aW50ZXIwMQ==", "IDMRW2": "cmExbmZhbGw=", "SAILPOINT":"UVFLbGhhJDVqaVZxSnFl", "GROUPER": "OWM3bmg5Wnh0NDY2cUE1Rm1nUU1HeXln"}
elif env == "P-0":   #Pespu6Ex
    DBServerOld = 'dbprod1ox.iam.huit.harvard.edu:8506/iamprod'
    DBPermissions = {"IAMAUTOQA": "UGVzcHU2RXg=", "CLAIM_ACCOUNT": "", "IDMRW": "", "IDMRW2": ""}

#------- LOGGING SETUP -------
if not os.path.exists(LogFolder):
    os.makedirs(LogFolder)
        
logging.basicConfig(level=logging.INFO,
                    format='%(message)s',
                    datefmt='%m-%d %H:%M',
                    filename=LogFolder + logfile,
                    filemode='w')

console = logging.StreamHandler()
console.setLevel(logging.INFO)

formatter = logging.Formatter('%(message)s')
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)
logger1 = logging.getLogger('Info')
logger2 = logging.getLogger('Error')


def DatabaseOperations(server, userName, DbPassword, query):
    useHeaders = False
    headerValues = []
    queryType = ""
    
    if server == "" or userName == "" or DbPassword == "" or query == "":
        print("insufficient information")
        return
    else:
        connectString = userName + "/" + DbPassword + "@" + server
        #set header values    
        outputProcessing = "SET HEADING OFF TRIM ON TRIMSPOOL ON PAGESIZE 2000 LINESIZE 130 COLSEP : RECSEPCHAR | RECSEP EACH NULL NULL UND OFF FEED OFF\n "    

        queryResult = submitOracleQuery(connectString, outputProcessing, query)
        
    return queryResult

  
def submitOracleQuery(connectString, outputProcessing, query):
    session = Popen(['/opt/oracle/instantclient/sqlplus', '-S', connectString], stdin=PIPE, stdout=PIPE, stderr=PIPE)
    session.stdin.write(bytes(outputProcessing + query, 'UTF-8'))
    queryResult, errorMessage = session.communicate()
    if errorMessage.decode("utf-8").strip() != "":
        UTILS.ReportErrors("Database Query failed: " + str(errorMessage.decode("utf-8").strip()), record)
        return errorMessage
    elif queryResult != "":
        queryResult = queryResult.decode("utf-8").strip()  # .replace("-","").replace("   ", "").replace("  ", "").replace("\t", "\n").replace("\n\n", "").replace("\n", " : ")
        queryResult = re.sub(r"[|+]+", "|", queryResult).strip()
        queryResult = re.sub(r"(\-\-)+", "", queryResult)
        queryResult = re.sub(r"[\n\t]+", ":", queryResult)
        queryResult = re.sub(r"(\s\s+)+", "", queryResult)
        queryResult = re.sub(r"(\s\t+)+", "", queryResult)
        queryResult = re.sub(r"(::)+", ":", queryResult)
        queryResult = queryResult.replace(":|:", ":")
        try:
            if queryResult[-1] == "|":
                queryResult = queryResult[0:-1]
            if queryResult[-1] == ":":
                queryResult = queryResult[0:-1]
        except:
            pass
        queryResult = queryResult.strip()
        #print(queryResult)
    return queryResult

def DatabaseList(server, userName, DbPassword, query):
    useHeaders = False
    headerValues = []
    queryType = ""
    
    if server == "" or userName == "" or DbPassword == "" or query == "":
        print("insufficient information")
        return
    else:
        connectString = userName + "/" + DbPassword + "@" + server
        #set header values    
        outputProcessing = "SET HEADING OFF TRIM ON TRIMSPOOL ON PAGESIZE 2000 LINESIZE 130 COLSEP : RECSEPCHAR | RECSEP EACH NULL NULL UND OFF FEED OFF\n "    

        queryResult = submitOracleQuery2(connectString, outputProcessing, query)
        
    return queryResult

  
def submitOracleQuery2(connectString, outputProcessing, query):
    session = Popen(['/opt/oracle/instantclient/sqlplus', '-S', connectString], stdin=PIPE, stdout=PIPE, stderr=PIPE)
    session.stdin.write(bytes(outputProcessing + query, 'UTF-8'))
    queryResult, errorMessage = session.communicate()
    if errorMessage.decode("utf-8").strip() != "":
        UTILS.ReportErrors("Database Query failed: " + str(errorMessage.decode("utf-8").strip()), record)
        return errorMessage
    elif queryResult != "":
        queryResult = queryResult.decode("utf-8").strip()  # .replace("-","").replace("   ", "").replace("  ", "").replace("\t", "\n").replace("\n\n", "").replace("\n", " : ")
        queryResult = re.sub(r"[|+]+", "|", queryResult).strip()
        #queryResult = re.sub(r"(\-\-)+", "", queryResult)
        queryResult = re.sub(r"[\n\t]+", ":", queryResult)
        queryResult = re.sub(r"N:: : :::U:: : :::L:: : :::L", "NULL", queryResult)
        queryResult = re.sub(r"::", ":", queryResult)
        queryResult = re.sub(r"(\s\s+)+", "", queryResult)
        queryResult = queryResult.split(":|:")
        #print(queryResult)
        #print(len(queryResult))
        counter = 0
        for entry in queryResult:
            entry = entry.replace(":|", "")
            queryResult[counter] = entry.split(":")
            counter += 1
    return queryResult


            
def main():
    verboseReporting = False
    dictObject = ""
    
    logger1.info("\033[4m" + "Record Counts by Schema and Table" + "\033[0m" + "\n")
    logger1.info("OLD: " + DBServerOld)
    logger1.info("NEW: " + DBServerNew)
    
    for schemaOwner in schemaList:
        # get list of tables
        tableQuery = "SELECT table_name FROM all_tables WHERE owner = '" + schemaOwner + "';"
        userName = "IAMAUTOQA"
        
        try:
            DbPassword = base64.b64decode(bytes(DBPermissions[userName.upper()], "utf-8")).decode('ascii') 
        except UnicodeDecodeError:
            print("Invalid Password.  Test application will exit.")
            sys.exit()
            
        tableSet = str(DatabaseOperations(DBServerOld, userName, DbPassword, tableQuery)).split(":")
        tableSet.sort()

        maxLength = len(max(tableSet, key=len))
        discrepancyList = []
        discrepancyList2 = []
                
        logger1.info("\n" + ("Total Record Count").ljust(maxLength+20))
        logger1.info(indent + (schemaOwner + " Table").ljust(maxLength+8) + "Match".ljust(11) + " OLD".ljust(11) + " NEW".ljust(11))
        logger1.info(indent + ("---------------").ljust(maxLength+7) + "--------".ljust(11) + "--------".ljust(11) + "--------".ljust(11))

        for table in tableSet:
            if table.find("_HIST") > 0 or table.find("CHANGELOG") > 0 or table.find("_LOG") > 0 or table.find("_TMP") > 0:
                continue
            
            indexNumber = tableSet.index(table)
            selectQuery = "Select count(*) from " + schemaOwner + "." + table + ";"
            count1 = DatabaseOperations(DBServerOld, userName, DbPassword, selectQuery)
            count2 = DatabaseOperations(DBServerNew, userName, DbPassword, selectQuery)
            if (count1.find("table or view does not exist") and count2.find("table or view does not exist")) > -1:
                continue
            
            if count1 == count2: 
                matchValue = "YES"
            else: 
                matchValue = "NO"
                
                discrepancyList.append(table)
            
            comment = " " 
            if count2 > count1:
                comment = "NOTE: NEW > OLD"

            logger1.info(indent + table.ljust(maxLength+8)+ matchValue.ljust(11) + str(count1).ljust(11) + str(count2).ljust(11) + indent + comment)


        if len(discrepancyList) > 0 and dateValue[schemaOwner] != "none":
            logger1.info ("\n")
            logger1.info(("Most Recent Two Weeks").ljust(maxLength+25) + " OLD".ljust(15) + " NEW".ljust(15))
            logger1.info(indent + (schemaOwner + " Table").ljust(maxLength+5) + "Match".ljust(12) + "Past 2 Weeks".ljust(15) + "Past 2 Weeks".ljust(15))
            logger1.info(indent + ("---------------").ljust(maxLength+5) + "-----".ljust(12) + "------------".ljust(15) + "------------".ljust(15))

            recordCounter = 0
            for table in discrepancyList:
                columnCheckQuery = "SELECT COUNT(*) FROM ALL_TAB_COLUMNS WHERE OWNER = '" + schemaOwner + "' AND TABLE_NAME = '" + table + "' AND COLUMN_NAME = '" + dateValue[schemaOwner] +  "';"
                if str(DatabaseOperations(DBServerOld, userName, DbPassword, columnCheckQuery)) == "0":
                    logger1.info(indent + table.ljust(maxLength+5)+ "UNKNOWN".ljust(16) + "NO SYSDATE COLUMN TO COMPARE".ljust(15))
                    continue
                
                #try:
                partialCount1 = 0
                partialCount2 = 0
                partialCountQuery = "Select count(*) from " + schemaOwner + "." + table + " where " + dateValue[schemaOwner] + " >= sysdate - 14;"
                partialCount1 = (DatabaseOperations(DBServerOld, userName, DbPassword, partialCountQuery))
                partialCount2 = (DatabaseOperations(DBServerNew, userName, DbPassword, partialCountQuery))

                if schemaOwner == "XIDAPPS":
                    print(partialCount1)
                    print(partialCount2)

                if partialCount1 == partialCount2: 
                    matchValue = "YES"
                else: 
                    matchValue = "NO"
                    discrepancyList2.append(table)
                print(partialCount1)
                print(partialCount2)
                if partialCount1 > 0 or partialCount2 > 0:
                    recordCounter += 1

                comment = " " 
                if partialCount2 > partialCount1:
                    comment = "NOTE: NEW > OLD"

                logger1.info(indent + table.ljust(maxLength+5)+ matchValue.ljust(16) + str(partialCount1).ljust(15) + str(partialCount2).ljust(15) + indent + comment)
                #except:
                #    logger1.info("\n\n Unable to connect to " + schemaOwner)
                    
                    
            logger1.info("\n")
            
            if recordCounter > 0:
                logger1.info("Records missing from new " + schemaOwner + " schema:")

                for table in discrepancyList2:
                    #try:
                    partialCountQuery = "Select count(*) from " + schemaOwner + "." + table + " where " + dateValue[schemaOwner] + " >= sysdate - 14;"
                    partialCount1 = int(DatabaseOperations(DBServerOld, userName, DbPassword, partialCountQuery))
                    partialCount2 = int(DatabaseOperations(DBServerNew, userName, DbPassword, partialCountQuery))
                    #except:
                    #    logger1.info("\n\n Unable to connect to " + schemaOwner)


                    primaryKeyQuery = "SELECT count(column_name) from all_tab_columns WHERE owner = '" + schemaOwner + "' and table_name = '" + table + "' and column_name = 'INTERNAL_ID';"
                    primaryKeyValue = DatabaseOperations(DBServerNew, userName, DbPassword, primaryKeyQuery)
                    if int(primaryKeyValue) > 0 and schemaOwner in ["IDMRW", "IDMRW2", "SAILPOINT"]:
                        primaryKeyValue = "INTERNAL_ID"
                    else: 
                        primaryKeyQuery = "SELECT column_name FROM all_cons_columns WHERE owner = '" + schemaOwner + "' and constraint_name = (SELECT constraint_name FROM all_constraints WHERE owner = '" + schemaOwner + "' and UPPER(table_name) = UPPER('" + table + "') AND (CONSTRAINT_TYPE = 'P' OR CONSTRAINT_NAME = 'INTERNAL_ID'));"
                        primaryKeyValue = str(DatabaseOperations(DBServerNew, userName, DbPassword, primaryKeyQuery))


                    discrepancyQuery = "Select " + primaryKeyValue + "," + dateValue[schemaOwner] + " from " + schemaOwner + "." + table + " where " + dateValue[schemaOwner] + " >= sysdate - 14 order by " + dateValue[schemaOwner] + ";"
                    discrepancyList1 = DatabaseList(DBServerOld, userName, DbPassword, discrepancyQuery)
                    discrepancyList2 = DatabaseList(DBServerNew, userName, DbPassword, discrepancyQuery)

                    logger1.info("\n")
                    logger1.info(indent + schemaOwner + "." + table)
                    logger1.info((indent)*2 + primaryKeyValue.ljust(35) + dateValue[schemaOwner].ljust(16))
                    logger1.info((indent)*2 + "-----------------".ljust(35) + "----------------------------".ljust(16))
                    for record in discrepancyList1:
                        if partialCount1 <= partialCount2:
                            break
                        else:
                            if record not in discrepancyList2:  
                                logger1.info((indent)*2 + record[0].ljust(35) + record[1].ljust(16))
                                partialCount1 += -1

                    
# -----------------------------------------------------------------------------

if __name__ == "__main__":
    main()
    